#!/bin/bash

# Set default values
APP_JSON_PATH="/app/universis-api/dist/server/config/app.json"
TMP_JSON_PATH="/app/universis-api/dist/server/config/tmp_app.json"

# Copy the original app.json to a temporary file
cp $APP_JSON_PATH $TMP_JSON_PATH

# Modify the temporary app.json file based on environment variables
jq --arg reports_server "${REPORTS_SERVER:-https://example.com/jasperserver/}" \
   --arg reports_user "${REPORTS_USER:-user}" \
   --arg reports_password "${REPORTS_PASSWORD:-password}" \
   --arg auth_unattended_execution_account "${AUTH_UNATTENDED_EXECUTION_ACCOUNT:-b86YUWP5SC7N==}" \
   --arg auth_server_uri "${AUTH_SERVER_URI:-https://auth.example.com}" \
   --arg auth_client_id "${AUTH_CLIENT_ID:-1234567890123}" \
   --arg auth_client_secret "${AUTH_CLIENT_SECRET:-Bh8BKWsdH5TdCNShBpq7DoMSfeJHmeDBupRa}" \
   --arg auth_callback_uri "${AUTH_CALLBACK_URI:-http://localhost:5001/auth/callback}" \
'.settings.universis.reports.server = $reports_server |
.settings.universis.reports.user = $reports_user |
.settings.universis.reports.password = $reports_password |
.settings.auth.unattendedExecutionAccount = $auth_unattended_execution_account |
.settings.auth.server_uri = $auth_server_uri |
.settings.auth.client_id = $auth_client_id |
.settings.auth.client_secret = $auth_client_secret |
.settings.auth.callback_uri = $auth_callback_uri' $TMP_JSON_PATH > $APP_JSON_PATH

# Check if database environment variables exist
if [ -z "$DB_SERVER" ] && [ -z "$DB_USER" ] && [ -z "$DB_PASSWORD" ] && [ -z "$DB_DATABASE" ]
then
    jq --arg invariant_name "sqlite" \
       --arg db_database "db/universisWorking.db" \
    '.adapters[0].invariantName = $invariant_name |
    .adapters[0].options.database = $db_database' $APP_JSON_PATH > $TMP_JSON_PATH
else
    jq --arg db_invariant_name "${DB_INVARIANT_NAME:-mssql}" \
       --arg db_server "${DB_SERVER}" \
       --arg db_user "${DB_USER}" \
       --arg db_password "${DB_PASSWORD}" \
       --arg db_database "${DB_DATABASE}" \
    '.adapters[0].invariantName = $db_invariant_name |
    .adapters[0].options.server = $db_server |
    .adapters[0].options.user = $db_user |
    .adapters[0].options.password = $db_password |
    .adapters[0].options.database = $db_database' $APP_JSON_PATH > $TMP_JSON_PATH
fi

# Copy the final version of the temporary app.json file back to the original location
cp $TMP_JSON_PATH $APP_JSON_PATH
