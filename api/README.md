# Universis-API

## What is Universis ?

Redesigning our universities universe !

UniverSIS is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

This image is based on the [Universis-API repository](https://gitlab.com/universis/universis-api).

## How to use this image

### Default image configuration

Start by running the following command:

`$ docker run -d --name api -p 5001:5001 universis-api`

This command initiates a container that exposes the API's default port 5001, utilizing the universis-api image.

### Environment variable configuration

If you wish to use a custom configuration for running the container, you can specify environment variables as per your requirements:

`$ docker run -d --name api -p 5001:5001 -e DB_SERVER=my.database.server universis-api`

### Environment list variable configuration

Alternatively, you can use a _.list_ file to manage the environment variables for your container. Here's the default .list file:

```
DB_SERVER=my.database.server
DB_USER=my_database_user
DB_PASSWORD=my_database_password
DB_DATABASE=my_database
DB_INVARIANT_NAME=mssql

UNIVERSIS_REPORTS_SERVER=https://example.com/jasperserver/
UNIVERSIS_REPORTS_USER=user
UNIVERSIS_REPORTS_PASSWORD=password

UNATTENDED_EXECUTION_ACCOUNT=b86YUWP5SC7N==
SERVER_URI=https://auth.example.com
CLIENT_ID=1234567890123
CLIENT_SECRET=Bh8BKWsdH5TdCNShBpq7DoMSfeJHmeDBupRa
CALLBACK_URI=http://localhost:5001/auth/callback
```

_INVARIANT_NAME : mssql, sqlite, mysql, postgres, oracle_

Given this file is named **env.list**, you can use the following command to start the container:

`$ docker run -d --name api -p 5001:5001 --env-file env.list universis-api`
