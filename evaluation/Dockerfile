# Build
FROM node:14.21.3-slim AS builder

WORKDIR /app

RUN apt-get update && \
    apt-get install -y python2 make g++ && \
    ln -s /usr/bin/python2 /usr/bin/python

COPY ./evaluation-app/ .

RUN npm install -g @angular/cli@6 && \
    npm install && \
    npm run build -- --configuration production --base-href "/evaluation/"

# Deploy
FROM nginx:1.25.1

RUN apt-get update \
    && apt-get install -y jq \
    && rm -rf /var/lib/apt/lists/*
    
COPY --from=builder /app/dist /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

COPY modify_app_json.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/modify_app_json.sh

CMD ["/bin/bash", "-c", "/usr/local/bin/modify_app_json.sh && nginx -g 'daemon off;'"]

EXPOSE 4200
