# Universis-Admin

## What is Universis ?

Redesigning our universities universe !

UniverSIS is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

This image is based on the [Universis Admin repository](https://gitlab.com/universis/universis-admin).

## How to use this image

### Default image configuration

Start by running the following command:

`$ docker run -d --name admin -p 4200:4200 universis-admin`

This command initiates a container that exposes the Student's frontend on default port 4200, utilizing the universis-admin image.

### Environment variable configuration

If you wish to use a custom configuration for running the container, you can specify environment variables as per your requirements:

`$ docker run -d --name admin -p 4200:4200 -e SERVER=https://api.universis.io/api/ universis-admin`

### Environment list variable configuration

Alternatively, you can use a _.list_ file to manage the environment variables for your container. Here's the default .list file:

```
SERVER=https://api.universis.io/api/
AUTHORIZE_URL=https://users.universis.io/authorize
LOGOUT_URL=https://users.universis.io/logout?continue=http://localhost:4200/#/auth/login
USER_PROFILE_URL=https://users.universis.io/me
TOKEN_URL=https://users.universis.io/tokeninfo
CLIENT_ID=6065706863394382
CALLBACK_URL=http://localhost:4200/auth/callback/
SCOPE=students
```

Given this file is named **env.list**, you can use the following command to start the container:

`$ docker run -d --name admin -p 4200:4200 --env-file env.list universis-admin`
