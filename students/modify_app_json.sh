#!/bin/bash

# Set default values
APP_JSON_PATH="/usr/share/nginx/html/assets/config/app.json"
TMP_JSON_PATH="/usr/share/nginx/html/assets/config/tmp_app.json"

# Copy the original app.json to a temporary file
cp $APP_JSON_PATH $TMP_JSON_PATH

# Modify the temporary app.json file based on environment variables
jq --arg server "${SERVER:-https://api.universis.io/api/}" \
   --arg authorize_url "${AUTHORIZE_URL:-https://users.universis.io/authorize}" \
   --arg logout_url "${LOGOUT_URL:-https://users.universis.io/logout?continue=http://localhost:7001/#/auth/login}" \
   --arg user_profile_url "${USER_PROFILE_URL:-https://users.universis.io/me}" \
   --arg token_url "${TOKEN_URL:-https://users.universis.io/tokeninfo}" \
   --arg client_id "${CLIENT_ID:-6065706863394382}" \
   --arg callback_url "${CALLBACK_URL:-http://localhost:7001/auth/callback/}" \
   --arg scope "${SCOPE:-students}" \
'.settings.remote.server = $server |
.settings.auth.authorizeURL = $authorize_url |
.settings.auth.logoutURL = $logout_url |
.settings.auth.userProfileURL = $user_profile_url |
.settings.auth.oauth2.tokenURL = $token_url |
.settings.auth.oauth2.clientID = $client_id |
.settings.auth.oauth2.callbackURL = $callback_url |
.settings.auth.oauth2.scope[0] = $scope' $TMP_JSON_PATH > $APP_JSON_PATH

# Remove the temporary app.json file
rm $TMP_JSON_PATH

